import Vue from 'vue'
import Router from 'vue-router'
import BaseView from '@/components/BaseView'
import Lobby from '@/components/Lobby'

Vue.use(Router)

export default new Router({
    routes: [{
        path: '/',
        name: 'Main',
        component: Lobby,
        redirect: '/lobby'
    }, {
        path: '/lobby',
        name: 'Lobby',
        component: Lobby
    }, {
        path: '/:sceneId',
        name: 'BaseView',
        component: BaseView
    }],
    mode: 'history'
})
