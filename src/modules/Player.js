import Entity from './engine/Entity'
import * as THREE from 'three';

import Bomb from './Bomb';

class Player extends Entity {

    xVelocity = 0;
    yVelocity = 0;

    bombRadius = 3;

    raycaster;
    clock;

    constructor(scene) {
        super(scene, 'Player');

        this.speed = 8;
        this.raycaster = new THREE.Raycaster();
        this.clock = new THREE.Clock(true);
    }

    GenerateModel() {
        const geometry = createBoxWithRoundedEdges(2.5, 2.5, 2.5, 1, 1);
        const material = new THREE.MeshBasicMaterial({ color: '#03b1fc' });
        const cube = new THREE.Mesh(geometry, material);

        return cube;
    }

    Start() {

    }

    Update() {
        this.Move();

        if (!this.isMine) return;
    }

    OnKeyDown(e) {
        if (!this.isMine) return;

        const oldXVelocity = this.xVelocity;
        const oldYVelocity = this.yVelocity;
        let newXVelocity = this.xVelocity;
        let newYVelocity = this.yVelocity;

        switch (e.key) {
            case 'w':
                newYVelocity = 1;
                break;
            case 'a':
                newXVelocity = -1;
                break;
            case 's':
                newYVelocity = -1;
                break;
            case 'd':
                newXVelocity = 1;
                break;
        }

        const canMoveIntoDirection = !this.DetectCollisions(newXVelocity, newYVelocity);
        const hasChangedDirection = (oldXVelocity !== newXVelocity || oldYVelocity !== newYVelocity);

        if (!canMoveIntoDirection) {
            newXVelocity = 0;
            newYVelocity = 0;
        }

        if (canMoveIntoDirection && hasChangedDirection) {
            this.xVelocity = newXVelocity;
            this.yVelocity = newYVelocity;

            this.scene.RPC(this, 'RPC_UpdateVelocity', 0, { xVelocity: newXVelocity, yVelocity: newYVelocity })
        }

        if (e.key === ' ') this.PlaceBomb();
    }

    OnKeyUp(e) {
        if (!this.isMine) return;
        if (e.key === 'p') this.bombRadius = 10;
        switch (e.key) {
            case 'w':
                this.yVelocity = 0;
                break;
            case 'a':
                this.xVelocity = 0;
                break;
            case 's':
                this.yVelocity = 0;
                break;
            case 'd':
                this.xVelocity = 0;
                break;
        }

        this.scene.RPC(this, 'RPC_UpdateVelocity', 0, { xVelocity: this.xVelocity, yVelocity: this.yVelocity })
    }

    DetectCollisions(xDirection, yDirection) {
        const direction = new THREE.Vector3(xDirection, yDirection);
        const position = this.model.position;

        this.raycaster.set(position, direction);
        this.raycaster.far = 1;

        const walls = this.scene.entities
            .filter(entity => entity.constructor.name !== 'Player' && entity.constructor.name !== 'Bomb')
            .map(entity => entity.model)
        const intersects = this.raycaster.intersectObjects(walls);

        return intersects.length > 0;
    }

    Move() {
        const canMoveIntoDirection = !this.DetectCollisions(this.xVelocity, this.yVelocity);

        if (canMoveIntoDirection) {
            const deltaTime = this.clock.getDelta();

            this.model.position.x += this.xVelocity * deltaTime * this.speed;
            this.model.position.y += this.yVelocity * deltaTime * this.speed;
        }
    }

    PlaceBomb() {
        const bomb = new Bomb(this.scene, this.bombRadius);
        this.scene.Instantiate(bomb, this.model.position.x, this.model.position.y);
    }

    Destroy() {
        super.Destroy();

        this.scene.config.wrapper.LoadLobby();
    }

    RPC_UpdateVelocity(translation) {
        const { xVelocity, yVelocity } = translation;

        this.xVelocity = xVelocity;
        this.yVelocity = yVelocity;
    }
}

const createBoxWithRoundedEdges = (width, height, depth, radius0, smoothness) => {
    let shape = new THREE.Shape();
    let eps = 0.00001;
    let radius = radius0 - eps;
    shape.absarc(eps, eps, eps, -Math.PI / 2, -Math.PI, true);
    shape.absarc(eps, height - radius * 2, eps, Math.PI, Math.PI / 2, true);
    shape.absarc(width - radius * 2, height - radius * 2, eps, Math.PI / 2, 0, true);
    shape.absarc(width - radius * 2, eps, eps, 0, -Math.PI / 2, true);
    let geometry = new THREE.ExtrudeBufferGeometry(shape, {
        amount: depth - radius0 * 2,
        bevelEnabled: true,
        bevelSegments: smoothness * 2,
        steps: 1,
        bevelSize: radius,
        bevelThickness: radius0,
        curveSegments: smoothness
    });

    geometry.center();

    return geometry;
}

export default Player;
