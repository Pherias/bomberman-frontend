import Scene from './engine/Scene';
import * as THREE from 'three';

import Player from './Player';
import Wall from './Wall'
import DestroyableWall from './DestroyableWall'
import Bomb from './Bomb'

const MAP_SIZE = 15;
const BLOCK_SIZE = 3;
const SPAWN_POSITIONS = [
    { x: -20, y: 16 },
    { x: 17, y: 16 },
    { x: -20, y: -20 },
    { x: 17, y: -20 }
]

class World extends Scene {

    localPlayer;

    constructor(config) {
        const registry = {
            Player,
            Wall,
            DestroyableWall,
            Bomb
        }
        super(config, registry);
    }

    OnConnect(connections) {
        const { x, y } = this.GetSpawnpoint(connections);
        this.localPlayer = new Player(this);
        this.Instantiate(this.localPlayer, x, y);
    }

    LoadScene() {
        super.LoadScene();

        this.camera.position.set(-1, -1, 30);

        this.scene.background = new THREE.Color('#25de14')

        const geometry2 = new THREE.PlaneGeometry(90, 90, 1);
        const material2 = new THREE.MeshBasicMaterial({ color: '#25de14', side: THREE.DoubleSide });
        const plane = new THREE.Mesh(geometry2, material2);
        this.scene.add(plane);

        for (let x = 0; x < MAP_SIZE; x += 1) {
            for (let y = 0; y < MAP_SIZE; y += 1) {
                const placeCube = this.ShouldBeWall(x, y);
                const spawnX = (x * 3) - MAP_SIZE * 3 / 2;
                const spawnY = (y * 3) - MAP_SIZE * 3 / 2;
                const placeDestroyableCube = Math.random() > .3 && !this.IsNearSpawnpoint(spawnX, spawnY);

                if (!this.isMaster) continue;

                if (placeCube) {
                    this.PlaceWall(spawnX, spawnY)
                } else if (placeDestroyableCube)
                    this.PlaceDestroyableWall(spawnX, spawnY);
            }
        }
    }

    PlaceDestroyableWall(x, y) {
        const wall = new DestroyableWall(this);
        this.Instantiate(wall, x, y);
    }

    PlaceWall(x, y) {
        const wall = new Wall(this);
        this.Instantiate(wall, x, y);
    }

    ShouldBeWall(x, y) {
        return x === 0 ||
            x === MAP_SIZE - 1 ||
            y === 0 ||
            y === MAP_SIZE - 1 ||
            (x % 2 === 0 &&
                y % 2 === 0)
    }

    IsNearSpawnpoint(x, y) {
        return SPAWN_POSITIONS.some(point => getDistanceBetweenPoints(x, y, point.x, point.y) < 6)
    }

    GetSpawnpoint(connections) {
        const index = connections.length - 1;

        return SPAWN_POSITIONS[index]
    }

    Update() {

    }
}

const createCube = (color) => {
    const geometry = new THREE.BoxGeometry(3, 3);
    const material = new THREE.MeshBasicMaterial({ color });
    const cube = new THREE.Mesh(geometry, material);

    return cube;
}

const getDistanceBetweenPoints = (x1, y1, x2, y2) => {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
}

export default World;
