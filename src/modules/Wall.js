import Entity from './engine/Entity'
import * as THREE from 'three';

class Wall extends Entity {

    constructor(scene) {
        super(scene, 'Wall');
    }

    GenerateModel() {
        const geometry = new THREE.BoxGeometry(3, 3, 3);
        const material = new THREE.MeshBasicMaterial({ color: '#535353' });
        const cube = new THREE.Mesh(geometry, material);
        return cube;
    }

    Start() {

    }

    Update() {

    }
}

export default Wall;
