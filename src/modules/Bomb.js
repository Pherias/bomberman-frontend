import Entity from './engine/Entity'
import * as THREE from 'three';

const BOMB_EXPLOSION_TIME = 1800;

class Bomb extends Entity {

    radius;
    raycaster;

    constructor(scene, radius) {
        super(scene, 'Bomb');

        this.raycaster = new THREE.Raycaster();
        this.radius = radius;
    }

    GenerateModel() {
        const geometry = new THREE.SphereGeometry(1.2, 16, 16);
        const material = new THREE.MeshBasicMaterial({ color: 'black' });
        const sphere = new THREE.Mesh(geometry, material);

        return sphere;
    }

    Start() {
        this.scene.RPC(this, 'RPC_SetRadius', 0, this.radius)

        setTimeout(() => { this.Explode(); }, BOMB_EXPLOSION_TIME);
    }

    Update() {}

    Explode() {
        if (!this.isMine) return;

        setTimeout(() => {
            this.Destroy()
        }, 200);

        const directions = [
            new THREE.Vector3(1, 0),
            new THREE.Vector3(-1, 0),
            new THREE.Vector3(0, 1),
            new THREE.Vector3(0, -1)
        ]
        const position = this.model.position;

        let intersects = [];

        directions.forEach(direction => {

            this.raycaster.set(position, direction);
            this.raycaster.far = this.radius;

            const hitables = this.scene.entities
                .filter(entity => ['DestroyableWall', 'Player'].includes(entity.constructor.name))
                .map(entity => entity.model)

            if (this.raycaster.intersectObjects(hitables).length > 0)
                intersects = [...intersects, this.raycaster.intersectObjects(hitables)[0]]
        })

        const modelIds = intersects.map(intersect => intersect.object.uuid);

        const entities = this.scene.GetEntitiesByModelId(modelIds);

        entities.forEach(entity => entity.Destroy())
    }

    RPC_SetRadius(radius) {
        this.radius = radius;
    }
}

export default Bomb;
