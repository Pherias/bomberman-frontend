import * as THREE from 'three';
import Connection from './Connection';

class Scene {

    id;
    connection;
    scene;
    camera;
    renderer;

    pressing;
    config;

    isMaster;
    entities;

    constructor(config, registry) {
        this.config = config;
        this.id = config.sceneId;
        this.connection = new Connection(this);
        this.connection.Connect(config.endpoint)
            .then((connection) => {
                const { entitiesToInstantiate, isMaster, connections } = connection;

                this.isMaster = isMaster;

                this.LoadScene()
                this.OnConnect(connections);
                this.config.wrapper.OnConnected(this);

                entitiesToInstantiate.forEach(entity => this.RPC_Instantiate(entity))
            })

        this.registry = registry;

        this.entities = [];
        this.pressing = [];
    }

    LoadScene() {
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

        this.renderer = new THREE.WebGLRenderer({
            antialias: true
        });
        this.renderer.setSize(window.innerWidth - 50, window.innerHeight - 50);
        this.config.parent.appendChild(this.renderer.domElement);
    }

    OnConnect() {};

    Update() {};

    OnKeyDown() {};

    OnKeyUp() {}

    Instantiate(entity, xPosition, yPosition) {
        this.connection.Instantiate(entity, xPosition, yPosition);

        this.AddEntity(entity, true);

        entity.model.position.x = xPosition;
        entity.model.position.y = yPosition;

        return entity;
    }

    RPC_Instantiate(entityDefinition) {
        const { type, id, xPosition, yPosition } = entityDefinition;

        const Entity_ = this.registry[type];
        if (!Entity_) throw new Error(`'${type}' not found in scene registry`)

        const entity = new Entity_(this);
        entity.id = id;

        entity.model.position.x = xPosition;
        entity.model.position.y = yPosition;

        this.AddEntity(entity, false);
    }

    AddEntity(entity, isMine) {
        this.scene.add(entity.model);
        this.entities.push(entity);

        entity.isMine = isMine;
        entity.Start();
    }

    Destroy(entity) {
        this.connection.Destroy(entity.id);

        this.DestroyEntity(entity);
    }

    RPC_Destroy({ entityId }) {
        const entity = this.entities.find(entity => entity.id === entityId);

        this.DestroyEntity(entity);
    }

    DestroyEntity(entity) {
        entity.model.geometry.dispose();
        entity.model.material.dispose();

        this.scene.remove(entity.model);
        const entityIndex = this.entities.indexOf(entity);

        this.entities.splice(entityIndex, 1);
    }

    RPC(entity, action, bufferMode, args) {
        this.connection.RPC(entity, action, bufferMode, args)
    }

    RPC_Callback(entityId, action, args) {
        const entity = this.entities.find(entity => entity.id === entityId)

        if (!entity) throw new Error(`Could not run RPC '${action}' because entity ${entityId} was not found`)

        entity[action](args);
    }

    GetEntitiesOfType(type) {
        if (!this.entities) return [];

        return this.entities.filter(entity => entity.constructor.name === type);
    }

    GetEntitiesByModelId(modelIds) {
        return this.entities.filter(entity => modelIds.includes(entity.model.uuid))
    }

    GetEntityByModelId(modelId) {
        return this.entities.find(entity => modelId === entity.model.uuid)
    }
}

export default Scene;
