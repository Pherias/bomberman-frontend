import * as THREE from 'three';
import { v4 } from 'uuid';

class Entity {

    type;
    scene;
    model;
    isMine;

    constructor(scene, type) {
        if (!scene) throw new Error('no scene given in entity')
        if (!type) throw new Error('no type specified for entity')

        this.type = type;
        this.scene = scene;
        this.model = this.GenerateModel();
        this.id = v4();
    }

    Start() {}

    Update() {}

    OnKeyDown() {}

    OnKeyUp() {}

    OnKey(key) {
        return this.scene.pressing[key] === true;
    }

    Destroy() {
        this.scene.Destroy(this);
    }

    /**
     * Used to create a model which THREE.js can render into a scene
     */
    GenerateModel() {}
}

export default Entity;
