import Entity from './engine/Entity'
import * as THREE from 'three';

class DestroyableWall extends Entity {

    constructor(scene) {
        super(scene, 'DestroyableWall');
    }

    GenerateModel() {
        const geometry = new THREE.BoxGeometry(3, 3, 3);
        const material = new THREE.MeshBasicMaterial({ color: '#838383' });
        const cube = new THREE.Mesh(geometry, material);

        return cube;
    }

    Start() {

    }

    Update() {

    }
}

export default DestroyableWall;
